# marinetraffic.com to GPX

## Quick use with nix flakes

```fish
nix run 'git+https://gitlab.com/ThibaultLemaire/marinetraffic.com-to-gpx.git' -- dump_from_marinetraffic_com.json output.gpx
```

## About

marinetraffic.com let's you view a ship's past track, which is nice, but does not let save it in any way, which I don't think is very fair.
Especially since those tracks remain for like 6h or something?

Thankfully, some engineers do their job right, and the information is there, in a usable format.
You can just inspect the requests from your browser (`Ctrl+Shift+I` on Firefox, go to the `Network` tab), and look for something like this:

```http
https://www.marinetraffic.com/map/gettrackjson/shipid:665315/stdate:2023-04-22%2019:45/endate:2023-04-23%2019:45/trackorigin:livetrack
```

You can download the response (Firefox: Right-Click > Copy Value > Copy Response, and paste that into a text file), but that's JSON though, not GPX.

So I made this little tool to convert that JSON to GPX, making it usable by most software that manipulates GPS tracks.

I was recently on a ship, took some pictures, but forgot to turn on my GPS and record the track myself.
With this tool I was able to get the track and use it to geolocate the pictures in [Digikam](https://www.digikam.org/) by correlating the timestamps.
Plus, I also get to keep the GPX itself for later view.

I don't plan to get on a ship again any time soon, and I plan to _not_ forget to turn on my GPS next time, so... I consider this project complete for my purposes.

However, if you find it useful, and want to improve it, contributions are very welcome!
(I might also work on suggestions, depending on how big they are, how busy I am, and also my mood.)
