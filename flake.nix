{
  description = "marinetraffic.com JSON track to GPX converter";

  outputs = { self, nixpkgs }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in
    {

      packages.x86_64-linux.default = pkgs.callPackage ./derivation.nix { };

      devShells.x86_64-linux.default = with pkgs; mkShell {
        buildInputs = self.packages.x86_64-linux.default.propagatedBuildInputs ++ [
          nixpkgs-fmt
          black
        ];
      };
    };
}
