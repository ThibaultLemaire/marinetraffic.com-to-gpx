{ lib, python3Packages }:
with python3Packages;
buildPythonApplication {
  pname = "marinetraffic-com-to-gpx";
  version = "1.0";

  propagatedBuildInputs = [ gpxpy ];

  src = ./.;
}
