#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='marinetraffic-com-to-gpx',
      version='1.0',
      # Modules to import from other scripts:
      packages=find_packages(),
      # Executables
      scripts=["marinetraffic-com-to-gpx"],
     )
